#include "Map.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int Map::WIDTH = 0;
int Map::HEIGHT = 0;
int ** Map::matrix = nullptr;
int Map::nothing = 0;


struct Column2{
  public:
	
	int * line_ptr;

	Column2 (int* line_ptr) : line_ptr(line_ptr) {}
	
	inline int & operator[] (const int n){
		if (line_ptr == nullptr || n<0 || n>=Map::HEIGHT)
			return *(new int(0));													// POZOOOOOOOOOOOOORRRRRRRRR
			//return Objects::find_ID(0);
		return line_ptr[n];
		}

};


int & Map::at (const int x, const int y){
	if(x<0 || y<0 || x>=WIDTH || y>=HEIGHT)
		return *(new int(nothing));
	return matrix[x][y];
	}

Map::Map(){
	//HEIGHT=0; WIDTH=0;
	matrix=nullptr;
	}

Map::~Map(){
	for(int i=0; i<WIDTH; i++)
		delete matrix[i];
	delete matrix;
	}

inline Column2 Map::operator[] (const int n){	// UROB AKO TEMPLATE COLUMN kvoli dvom
	if(n<0 || n>=WIDTH)
		return Column2(nullptr);
	return Column2(matrix[n]);
	}

void Map::find_xy(const int ID, int & x, int & y){
	for(int i=0; i<WIDTH; i++)
		for(int j=0; j<HEIGHT; j++)
			if(matrix[i][j]==ID){
				x=i;
				y=j;
				return;
				}
	}

bool Map::load_file (const string & fname){
	//int ID, create_inv_ID, create_ID, create_X, create_Y;
	int i,j;
	string dump_the_utf8_beggining_bullshit;
	string message;
	istringstream iss;
	string line;
	ifstream file;
	file.open (fname, fstream::in);
	file.clear();
	if(!file.is_open())
		return false;
	
	for(int i=0; i<WIDTH; i++)
		delete matrix[i];
	delete matrix;

	std::getline(file, line);
	std::getline(file, line);
		iss.clear();
		iss.str(line.c_str());
		iss>>skipws>>WIDTH>>skipws>>HEIGHT;

	if(WIDTH<=0 || HEIGHT<=0){
		WIDTH=0; HEIGHT=0;
		file.close();
		return false;
		}

	matrix = new int* [WIDTH];
	for(i=0; i<WIDTH; i++)
		matrix[i]= new int[HEIGHT];

	j=-1;
	while(!file.eof() && ++j<HEIGHT){
		getline(file, line);
		if(line.length()<1)
			continue;
		iss.clear();
		iss.str(line.c_str());
		for(i=0; i<WIDTH; i++)
			iss>>skipws>>matrix[i][j];
        }

	file.close();
	return true;
	}

bool Map::save_file (const string & fname){
	ofstream file;
	file.open(fname);
	file<<"x,y size of the map\r\n";
	file<<WIDTH<<"\t"<<HEIGHT<<"\r\n";
	// KONTROLA OTVORENIA SUBORU
	for (int j=0; j<HEIGHT; j++){
		for(int i=0; i<WIDTH; i++)
			file<<matrix[i][j]<<"\t";
		file<<"\r\n";
		}
	file.close();
	return true;
	}