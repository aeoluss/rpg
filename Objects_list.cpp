#include "Object.h"
#include "Objects.h"
#include <curses.h>
#include "Objects_list.h"
#include <map>
#include <string>

using namespace std;

Objects_list::Objects_list(){
}

Objects_list::~Objects_list(){
}

string Objects_list::activate(){	
	int i=0;
	clean();
	for(map <int, Object> ::iterator it=Objects::objects.begin(); it!=Objects::objects.end(); ++it){
		printline(i++, it->second.icon+" "+it->second.name);
		}
	draw();
	getch();
	return "game";
	}