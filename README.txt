RPG - role playing terminal adventure game	{#mainpage}

Installation instructions - see INSTALL.txt

rpg.exe:
    game executable

editor.exe:
    used for converting <custom_map>.ed into <custom_map>.txt which is used in game

save files are stored in /files/<save_name>/*

For bug reporting, please send e-mail to peter@mikolaj.sk
