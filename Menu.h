#pragma once
#include <string>
#include "Screen.h"

///Menu screen, navigation (new game, load, save, exit..)
class Menu : public Screen {
  public:

  	///Game loaded indicator.
	static bool game_loaded;

	///Default constructor.
	Menu();
	///Default destructor.
	~Menu();
	std::string activate();
};
