#include <curses.h>
#include "Combat.h"
#include "Game.h"
#include <algorithm>
#include <sstream>
#include <unistd.h>
//#include <functional>

using namespace std;

Player * Combat::player = Player::Instance();
Stats * Combat::player_stats=nullptr;
Stats Combat::enemy_stats=Stats();
int Combat::difficulty_speed=10;		// steps per second
int Combat::difficulty_density=2;		// grossly every x iterations generate new char
int Combat::difficulty_width=WIDTH/2;	//42
int Combat::difficulty_height=HEIGHT;	//20
map<char, pair<int, int>> Combat::falling_chars = map <char, pair<int, int> > ();


Combat::Combat(){
}


Combat::~Combat(){
}

string Combat::activate(){
	bool evade;
	map<char, pair<int, int>>::iterator it;
	int offset_x=(WIDTH-difficulty_width)/2;
	int offset_y=(HEIGHT-difficulty_height)/2;
	player_stats=player->stats;
	enemy_stats=player->enemy->stats;		// KOPIRUJUCI KONSTRUKTOR
	clean();
	draw();
	falling_chars.clear();
	nodelay(stdscr,TRUE);				//non-blocking

	while(enemy_stats.hp>0 && player_stats->hp>0){
		if( (rand() % difficulty_density) == 0)
			falling_chars.insert( pair<char, pair<int, int>>( (char)(rand()%('a'-'z')+'A'), pair<int, int>(offset_x+rand()%difficulty_width, offset_y+0) ));
		clean();
		for(it=falling_chars.begin(); it!=falling_chars.end(); ++it)
			matrix[it->second.first][it->second.second]=it->first;
		for(int i=offset_x; i<(offset_x+difficulty_width); i++)
			matrix[i][offset_y+difficulty_height]='A';
		draw();

		player_hits(evade);
		if(evade) 
			return "animation-evade-game";
		show_health();
		draw();
		//flushinp();

		struct timespec timespec_key_delay;
		timespec_key_delay.tv_sec = 1000*player_stats->speed/difficulty_speed/enemy_stats.speed / 1000;
		timespec_key_delay.tv_nsec = (1000*player_stats->speed/difficulty_speed/enemy_stats.speed % 1000) * 1000000;
		nanosleep(&timespec_key_delay, nullptr);
		
		for(it=falling_chars.begin(); it!=falling_chars.end(); ++it){
			//fall((*it));
			it->second.second++;
			if(it->second.second >= (difficulty_height+offset_y) ){
				player_stats->hp-=enemy_stats.attack;
				falling_chars.erase(it->first);
				}
			}

		//for_each(falling_chars.begin(), falling_chars.end(), &fall );
		//for_each(falling_chars.begin(), falling_chars.end(), std::bind1st(std::mem_fun(&Combat::fall), this) );
		//std::bind1st(std::mem_fun(&C::addToSum),this)
		//mem_fun()
		}

	nodelay(stdscr,FALSE);					//blocking again

	if(player_stats->hp<=0)
		return "death";
	
	if(enemy_stats.hp<=0){
		Game::combat_succesfull=true;
		return "animation-combat_win-game";
		}

	return "game";
	}

void Combat::player_hits(bool & evade){
	char c;
	for(int i=0; i<10; i++){		// ALLOW KEYHITS PER STEP
		c=getch();					// MAKE NON-BLOCKING
		if(c==(char)27){
			evade=true;
			return;
			}
		c+='A'-'a';
		if(falling_chars.find(c)!=falling_chars.end())
			enemy_stats.hp-=player_stats->attack;
		falling_chars.erase(c);
		}
	}

void Combat::show_health(){
	ostringstream oss;
	string line;

	oss.str("");
	oss.flush();
	oss.clear();
	oss << enemy_stats.hp;
	line="HP enemy: ";
	line+=oss.str();
	status_bar(line);
	oss.str("");
	oss.clear();
	oss << player_stats->hp;
	line="HP player: ";
	line+=oss.str();
	status_bar(line);
	}