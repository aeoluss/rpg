#include "Player.h"

using namespace std;

Player* Player::p_instance = nullptr;


Player::Player() : stats(nullptr), enemy(nullptr){
	}

Player::~Player(){
	}

Player::Player(const Player & other) : stats(nullptr), enemy(nullptr) {}

Player & Player::operator=(const Player & other){ return *this; }

Player * Player::Instance(){
	if (p_instance == nullptr)
		p_instance = new Player();
	return p_instance;
	}

void Player::inventory_add (const int ID){
	if(ID!=0)
		inventory.push_back(ID);
	}