//#include "stdafx.h"
#include "Triggers.h"
#include "Trigger.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>

using namespace std;

/*void Triggers::activate (const int ID){
	cout << "aktivoval si " << ID << endl;
}
*/

map<int, Trigger> Triggers::triggers = map<int, Trigger>();

Triggers::Triggers(){
}

Triggers::~Triggers(){
}

Trigger * Triggers::find_ID (const int ID){
	/*for(map<int, Trigger>::iterator it = triggers.begin(); it!=triggers.end(); ++it)
		if(it->ID==ID)
			return (Trigger*)&(*it);
	return &triggers[0];*/
	if(triggers.find(ID)==triggers.end())
		return &(triggers.find(0)->second);
	return &(triggers.find(ID)->second);
	}

void Triggers::activate(int ID, const int object_x, const int object_y){
	/*for(map<int, Trigger>::iterator it = triggers.begin(); it!=triggers.end(); ++it){
		if(it->ID==ID)
			it->activate(object_x, object_y);
		}*/
	find_ID(ID)->activate(object_x, object_y);
	}

bool Triggers::load_file (const string & fname){
	int ID, create_inv_ID, create_ID, create_X, create_Y;
	string message="";
	istringstream iss;
	string line="";
	ifstream file;
	file.open (fname);
	if(!file.is_open())
		return false;
	getline(file, line);

	triggers.clear();

	triggers.insert(pair<int, Trigger>(0, Trigger(0, "", 0, 0, 0, 0)));
	while(!file.eof()){
		getline(file, line);
		if(line.length()<1)
			continue;
		iss.clear();
		iss.str(line.c_str());
		//message=line.substr(line.find_first_of('"'),line.find_last_of('"')-line.find_first_of('"'));		// ZRUS UVODZOVKY
		iss>>skipws>>ID>>skipws>>		message>>skipws>>      create_inv_ID>>skipws>>create_ID>>skipws>>create_X>>skipws>>create_Y;
		std::replace(message.begin(), message.end(), '_', ' ');
		triggers.insert (pair<int, Trigger> (ID, Trigger (ID, message, create_inv_ID, create_ID, create_X, create_Y) ) );
        }

	file.close();
	return true;
	}

bool Triggers::save_file (const string & fname){
	ofstream file;
	string message_to_write;
	file.open(fname);
	// KONTROLA OTVORENIA SUBORU
	map<int, Trigger>::const_iterator itE=triggers.end();
	file<<"ID	MESAGE	CinvID0	CreaID0	CreaX0	CreaY0\r\n";
	for(map<int, Trigger>::const_iterator it=triggers.begin(); it!=itE; ++it){
		message_to_write=it->second.message;
		std::replace(message_to_write.begin(), message_to_write.end(), ' ', '_');
		file<<it->second.ID<<"\t"<<message_to_write<<"\t"<<it->second.create_inv_ID<<"\t"<<it->second.create_ID<<"\t"<<it->second.create_x<<"\t"<<it->second.create_y<<"\r\n";
		}
	file.clear();

	file.close();
	return true;
	}