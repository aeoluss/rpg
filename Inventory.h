#pragma once
#include "Screen.h"
///Player inventory display
class Inventory : public Screen{
  public:

	///Default constructor.
	Inventory();
	///Default destructor.
	~Inventory();
	std::string activate();
};

