#pragma once
#include "Player.h"
#include <string>

///Trigger object used to make actions to make game dynamic/ storytelling
class Trigger{
  public:

	///Trigger ID, integer representation in database.
	int ID;
	///Message to be displayed.
	std::string message;
	///ID of an object to be created in player inventory when trigger is activated.
	int create_inv_ID;
	///ID of an object to be created on the map when trigger is activated.
	int create_ID;
	///x coord of an object to be created on the map.
	int create_x;
	///y coord of an object to be created on the map.
	int create_y;
	///Player singleton instance pointer.
	static Player * player;

	///Default constructor
	Trigger();
	/**
	*@brief
	*@param
	*@return
	*/
	Trigger(const int ID, const std::string & message, const int create_inv_ID, const int create_ID, const int create_X, const int create_Y);
	///Default destructor;
	~Trigger();
	/**
	*@brief				Activate the trigger (spawn item in player's inventory, and/or create an object on the map).
	*@param object_x	x coord of the object calling the trigger activation (if equal to 0, use predefined coordinates).
	*@param object_y	y coord of the object calling the trigger activation (if equal to 0, use predefined coordinates).
	*					Trigger creates an object in player's inventory, displays a message in the status_bar and creates an object
	*					on the map.
	*/
	void activate(const int object_x, const int object_y);
};
