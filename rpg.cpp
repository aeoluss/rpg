#include "Logic.h"
#include "Triggers.h"
#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef _WIN64
	#define CLS_COMMAND system("cls");
#elif _WIN32
	#define CLS_COMMAND system("cls");
#elif __linux
	#define CLS_COMMAND system("clear");
#elif __unix
	#define CLS_COMMAND system("clear");
#elif __posix
	#define CLS_COMMAND system("clear");
#else
	#define CLS_COMMAND system("clear");
#endif

//int main(int argc, _TCHAR* argv[])
///@file editor.cpp
int main(){
	Logic logic;
	logic.start();
	CLS_COMMAND
	cout<<"Thank you for playing."<<endl;
	return 0;
}