#pragma once
#include <string>

///Helper struct for operator[]
struct Column2;
/// Map screen, default game view
class Map{
  public:

  	///Height of the map.
	static int HEIGHT;
	///Width of the map.
	static int WIDTH;		// DOPLN CONST
	///2D Array of integers (object IDs) representing the map.
	static int ** matrix;
	///Integer value representing no object.
	static int nothing;

	///Default constructor.
	Map();
	///Default destructor.
	~Map();
	/**
	*@brief 		Accesses element of the map.
	*@param x		X coordinate of the element.
	*@param y		Y coordinate of the element.
	*@return		Element reference (integer reference).
	*/
	static int & at (const int x, const int y);
	///Operator[] for accessing element of matrix.
	inline Column2 operator[] (const int n);
	/**
	*@brief 		Loads data from file.
	*@param fname 	Relative path to file.
	*@return		Success reading file.
	*/
	static bool load_file (const std::string & fname);
	/**
	*@brief 		Saves data to file.
	*@param fname 	Relative path to file.
	*@return		Success writing file.
	*/
	static bool save_file (const std::string & fname);
	/**
	*@brief 		Finds first object by ID on the map.
	*@param ID		ID of the object to find.
	*@param x		x coordinate of the found object.
	*@param y		y coordinate of the found object.
	*/
	static void find_xy(const int ID, int & x, int & y);

};
