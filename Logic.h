#pragma once
#include "Screen.h"

///Automata switching between screens/ game modes (combat, map, inventory, menu)
class Logic{
public:
	//int difficulty;
	Screen *menu, *game, *combat, *animation, *inventory, *dialog, *objects_list;

	Logic();
	~Logic();
	void start();
};

