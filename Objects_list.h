#pragma once
#include "Screen.h"
#include <string>

///Helper class
class Objects_list : public Screen{
  public:
  	///Default constructor.
	Objects_list();
	///Default destructor.
	~Objects_list();
	std::string activate();
};

