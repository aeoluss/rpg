#include "Objects.h"
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

map <int, Object> Objects::objects = map <int, Object> ();

Objects::Objects(){
	}


Objects::~Objects(){

	}

Object * Objects::find_ID (const int ID){
	if(objects.find(ID)==objects.end())
		return &(objects.find(-1)->second);
	return &(objects.find(ID)->second);
	/*for(map<Object>::iterator it = objects.begin(); it!=objects.end(); ++it)
		if(it->ID==ID)
			return (Object*)&(*it);
	return &objects[0];
	*/
	}

/*Stats * Objects::get_stats_p(const int ID){
	return (& (find_ID(ID)->stat));
	}
	*/
bool Objects::load_file (const string & fname){
	int ID, hp, attack, speed, droprate, trigger, walkable, fightable;
	string icon;
	istringstream iss;
	string name, line;
	vector<int> triggers;
	ifstream file;
	objects.clear();

	file.open (fname);
	if(!file.is_open())
		return false;
	getline(file, line);
	
	objects.insert ( pair<int, Object> (0, Object (0, " ", "nothing", 0, 0, Stats(0, 0, 0, 0), vector<int>()) ));
	while(!file.eof()){
		getline(file, line);
		if(line.length()<1)
			continue;
		iss.clear();
		triggers.clear();
		iss.str(line.c_str());
		iss>>skipws>>ID>>icon>>skipws>>name>>skipws>>walkable>>skipws>>fightable>>skipws>>hp>>skipws>>attack>>skipws>>speed>>skipws>>droprate;
		while(iss.good()){
			iss>>skipws>>trigger;
			triggers.push_back(trigger);
			}
		objects.insert( pair<int, Object> (ID, Object (ID, icon, name, walkable, fightable, Stats(hp, attack, speed, droprate), triggers) ) );
        }

	file.close();
	return true;
	}

bool Objects::save_file (const string & fname){
	ofstream file;
	file.open(fname);
	// KONTROLA OTVORENIA SUBORU
	map<int, Object>::const_iterator itE=objects.end();
	file<<"ID	ICON	NAME	WALK	FIGHT	HP	ATTACK	SPEED	DROPR	TRImsg0 TRIG2=0\r\n";
	
	map<int, Object>::const_iterator it=objects.begin();
	++it;	// NOTHING
	for(; it!=itE; ++it){
//		file<<it->ID<<"\t";
//		file.write((char*) & it->icon, 4);
		file<<it->second.ID<<"\t"<<it->second.icon<<"\t"<<it->second.name<<"\t"<<it->second.walkable<<"\t"<<it->second.fightable<<"\t"<<it->second.stats.hp<<"\t"<<it->second.stats.attack<<"\t"<<it->second.stats.speed<<"\t"<<it->second.stats.droprate;
		for(vector<int>::const_iterator iTrig=it->second.triggers.begin(); iTrig!=it->second.triggers.end(); ++iTrig)
			file<<"\t"<<(*iTrig);
		file<<"\r\n";
		//file<<"\n";
		}
	file.close();
	return true;
	}