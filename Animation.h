#pragma once
#include "Video.h"
#include "Screen.h"
#include <map>
#include <string>

///Animation screen, used to play event animation or text animation
class Animation : public Screen{
  public:

  	///Map of loaded videos, key value is title of the video.
	static std::map <std::string, Video> videos;
	///Name of the video to be loaded.
	static std::string loaded_animation;
	///Command to be returned to main game logic after playing the video.
	static std::string return_command;

	///Default constructor.
	Animation ();
	///Default destructor.
	~Animation ();
	std::string activate ();
	/**
	*@brief 		Loads data from file.
	*@param fname 	Relative path to file.
	*@return		Success reading file.
	*/
	static bool load_file (const std::string & fname);
	/**
	*@brief 		Writes data to file.
	*@param fname 	Relative path to file.
	*@return		Success writing file.
	*/
	static bool save_file (const std::string & fname);
	/// Loads animation from animations loaded from file, using string command as a key.
	static void load (const std::string & command);
};

