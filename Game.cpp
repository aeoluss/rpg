#include "Game.h"
#include "Objects.h"
#include "Triggers.h"
#include "Map.h"
#include "Player.h"
#include <curses.h>
#include <unistd.h>

using namespace std;

int Game::key_delay = 100; // default 200000 in (microseconds)
int Game::fog=14;			// default 14
int Game::ratio_width=3;	// default 3
int Game::ratio_height=1;	// default 1
bool Game::combat_succesfull=false;
int Game::player_x=0;
int Game::player_y=0;
int Game::focus_x=0;
int Game::focus_y=0;
int Game::offset_x=0;
int Game::offset_y=0;
int Game::cursor_x=0;
int Game::cursor_y=0;
bool Game::first_fight=true;


Game::Game(){
}

Game::~Game(){
}

string Game::activate(){
	struct timespec timespec_key_delay;
    timespec_key_delay.tv_sec = key_delay / 1000;
    timespec_key_delay.tv_nsec = (key_delay % 1000) * 1000000;

	Player * player = Player::Instance();
	char com;
	int i, j;
player->stats= (& (Objects::find_ID(301)->stats) );
	if(combat_succesfull)
		goto AFTER_COMBAT;
	Map::find_xy(301, player_x, player_y);
	focus_x=player_x; focus_y=player_y;	// NESKOR ak by obrazovka isla mimo hraca
	offset_x=WIDTH/2;
	offset_y=HEIGHT/2;
	for(i=0; i<WIDTH; i++){
		for(j=0; j<HEIGHT; j++){
			if( ((offset_x-i)*(offset_x-i)/ratio_width+(offset_y-j)*(offset_y-j)/ratio_height) <= (fog*fog) ){
				matrix[i][j]=Objects::find_ID(Map::at(focus_x-offset_x+i,focus_y-offset_y+j))->icon[0];}
			 else{
				 matrix[i][j]=Objects::find_ID(Map::nothing)->icon[0];}
			}
		}
	draw();
	flushinp();
	nanosleep(&timespec_key_delay, nullptr);
	com=getch();
	cursor_x=player_x;
	cursor_y=player_y;
	if(com==27)
		return "menu";
	if(com=='o')
		return "objects";
	if(com=='i')
		return "inventory";
	if(com==4)	//if(com==KEY_LEFT)
		cursor_x--;
	if(com==3)	//if(com==KEY_UP)
		cursor_y--;
	if(com==5)	//if(com==KEY_RIGHT)
		cursor_x++;
	if(com==2)	//if(com==KEY_DOWN)
		cursor_y++;

	object= Objects::find_ID ( Map::at(cursor_x, cursor_y) );

	if(object->fightable){
		//player->stats= (& (Objects::find_ID(301)->stats) );
		player->enemy=object;
		//player->enemy_stats=object->stats;		// POZOR NA PRIRADZOVACI KONSTRUKTOR< ABY BOL KOPIRUJUCI
		if(first_fight){
			first_fight=false;
			return"animation-first_battle-combat";
			}
		return "animation-battle-combat";
		}

AFTER_COMBAT:
	combat_succesfull=false;

	action(object, cursor_x, cursor_y);

	if(object->walkable){
		Map::matrix[player_x][player_y]=1;
		player_x=cursor_x;
		player_y=cursor_y;
		Map::at(player_x, player_y)=301;
		}

	if(object->walkable && !object->fightable){		// is item
		//(*(player->stats))=(*(player->stats))+object->stats;
		//player->stats
		if(object->ID==666)
			return "animation-trap-animation-death-menu";

		bool l1, l2, l3;
		l1=object->stats.hp!=0;
		l2=object->stats.attack!=0;
		l3=object->stats.speed!=0;
		if(l1 || l2 || l3)
			player->inventory_add(object->ID);
/*
		(*(player->stats))+=object->stats;
		//string info=object->name;
		ostringstream iss;
		bool l1, l2, l3;
		l1=object->stats.hp!=0;
		l2=object->stats.attack!=0;
		l3=object->stats.speed!=0;
		iss.clear();
		if(l1 || l2 || l3) iss<<object->name;
		if(l1) iss<<"   HP: +"<<object->stats.hp;
		if(l2) iss<<"   attack: +"<<object->stats.attack;
		if(l3) iss<<"    speed: +"<<object->stats.speed;
		status_bar(iss.str());
*/
		}

	if(player->stats->hp<=0)
		return "death";

	return "game";
	}

void Game::action(Object *object, const int object_x, const int object_y){
	for(vector<int>::iterator it = object->triggers.begin(); it!=object->triggers.end(); ++it){
		if((*it)==0)
			continue;
		Triggers::activate(*it, object_x, object_y);
		}
	}
