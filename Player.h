#pragma once
#include "Stats.h"
#include "Object.h"

///Player object, stores extra information like stats, inventory, position..
class Player{
  public:
	///Stats pointer to the player object (ID 301).
	Stats * stats;
	///Enemy object pointer, used in duel.
	Object * enemy;
	///Vector of objects (their ID's) in player's inventory.
	std::vector <int> inventory;

	///Instance getter, returns the single instance pointer of player.
	static Player * Instance();
	/**
	*@brief			Adds item (ID) to player's inventory.
	*@param ID		ID of the item to be added.
	*/
	void inventory_add (const int ID);

private:
	///Singleton instance of the Player object.
	static Player * p_instance;
	///Default constructor made private.
	Player();
	///Default destructor made private.
	~Player();
	///Copy constructor.
	Player(Player const &);
	///Assignment operator.
	Player & operator=(const Player &);

};
