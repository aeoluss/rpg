#pragma once
#include <string>
#include <vector>
#include "Stats.h"

///Game object class. Represents every map object including player, serialized in file database
class Object{

public:
	///ID of an object.
	int ID;
	///Icon representation of the object on the map (String is used because of the UTF-8 chars).
	std::string icon;
	///Name of an object (for objects_list screen).
	std::string name;
	///Object is walkable flag.
	int walkable;
	///Object is fightable flag.
	int fightable;
	///Stats of an object.
	Stats stats;
	///List of triggers (their ID's) to be activated when interacted with.
	std::vector <int> triggers;

	/**
	*@brief 			Constructor for an object.
	*@param ID			ID of an object.
	*@param icon		Icon representation of the object on the map (String is used because of the UTF-8 chars).
	*@param name		Name of an object (for objects_list screen).
	*@param walkable	Object is walkable flag.
	*@param fightable	Object is fightable flag.
	*@param stats		Stats of an object.
	*@param triggers	List of triggers (their ID's) to be activated when interacted with.
	*/
	Object(const int ID, const std::string & icon, const std::string & name, const int walkable, const int fightable, const Stats & stats, const std::vector <int> & triggers);
	///Default destructor of an object.
	~Object();
};
