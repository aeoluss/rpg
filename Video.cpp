//#include "stdafx.h"
#include "Video.h"
#include <fstream>
#include <sstream>

using namespace std;

Video::Video(){
	fps=1;
}


Video::~Video(){
}

bool Video::load_file(const string & fname){
	ifstream file;
	istringstream iss;
	string line;
	list<string> fr;
	file.open(fname);
	if(!file.good())
		return false;
	//frames.clear();

	getline(file, line);
	getline(file, line);
	iss.str(line);
	iss>>skipws>>no_frames>>skipws>>x_size>>skipws>>y_size>>skipws>>fps;
	int counter=0;
	/*
	if(no_frames==0){
		while(!file.eof()){
			std::replace(message.begin(), message.end(), (char)(1), '\n');
			fr.clear();
			for(int i=0; i<y_size; i++){
				getline(file, line);
				fr.push_back(line);
				}
			frames.push_back(fr);
			}
		}
	*/
	while(!file.eof() && (counter++<no_frames || no_frames==0 ) ){
		fr.clear();
		for(int i=0; i<y_size; i++){
			getline(file, line);
			if(line[line.size()-1]=='\n')
				line.pop_back();
			if(line[line.size()-1]=='\r')
				line.pop_back();
			fr.push_back(line);
			}
		frames.push_back(fr);
		}

	return true;
	}

bool Video::save_file(const string & fname){
	ofstream file;
	string line;
	list<string> fr;
	file.open(fname);
	if(!file.good())
		return false;
	file<<"evade no_of_frames x y\r\n";
	file<<no_frames<<x_size<<y_size<<"\r\n";
	for(list<list<string>>::iterator it=frames.begin(); it!=frames.end(); ++it)
		for(list<string>::iterator itS=it->begin(); itS!=it->end(); ++itS)
			file<<(*itS)<<"\r\n";

	file.close();
	
	return true;
	}