#include "Screen.h"
#include <curses.h>
#include <stdio.h>
using namespace std;

int Screen::WIDTH = 72;
int Screen::HEIGHT = 22;
char ** Screen::matrix = nullptr;
string Screen::message1="";
string Screen::message2="";

struct Column{
  public:
	char * line_ptr;

	Column (char * line_ptr) : line_ptr(line_ptr) {}
	
	inline char & operator[] (const int n){
		return line_ptr[n];
		}

};

Column Screen::operator[] (const int n){
	return Column(matrix[n]);
	}

Screen::Screen(){
	int i;
	initscr();
	getmaxyx (stdscr, HEIGHT, WIDTH);
	WIDTH-=7;
	matrix = (new char*[WIDTH]);
	wrefresh (stdscr);
	for (i=0; i<WIDTH; i++)
		matrix[i] = new char[HEIGHT];
	clean();
	}
Screen::~Screen(){
	for (int i=0; i<WIDTH; i++)
		delete matrix[i];
	delete matrix;
	}

void Screen::clean(){
	for (int i=0; i<WIDTH; i++)
		for (int j=0; j<HEIGHT; j++)
			matrix[i][j]=' ';
	}

void Screen::draw()const{
	clear();
	string line="";
//	string refline="chary: ";
/*	char cr;
	for (int xz=0; xz<82; xz++){
		cr=(char)xz;
		refline+=xz;
		}
*/
//	printline(HEIGHT-3, refline);
	printline(HEIGHT-2, message1);
	printline(HEIGHT-1, message2);
	for (int j=0; j<HEIGHT; j++){
		for (int i=0; i<WIDTH; i++)
			line+=matrix[i][j];
		line+='\n';
		printw(line.c_str());
		line.clear();
		}
	
	wrefresh(stdscr);
	line.clear();
	}

void Screen::printline (const int line, const string & text, const int offset){
	for (int i=offset; i<( (int)text.length()+offset) && i>=0 && i<WIDTH && line>=0 && line<HEIGHT; i++)
		matrix[i][line]=text[i-offset];
	}

void Screen::status_bar(const string & message){
	if (message==message2)
		return;
	message1=message2;
	message2=message;
	}

string Screen::dialog (const string & text){
	string line;
	int i;
	char c;
	clean();
	printline(0, text);
	draw();
	line.clear();
	i=4;
	do{
		c=mvgetch(1,i);
		if (c==127){
			if (i==4)
				continue;
			line.pop_back();
			i--;
			matrix[i][1]=' ';
			move(1,i);
			draw();
			continue;
			}
		matrix[i++][1]=c;
		draw();
		line+=c;			// POSLEDNY NEWLINE
		}
	 while(c!='\n');
	line=line.substr(0, line.size()-1);
	return line;
	}
