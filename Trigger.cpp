#include "Trigger.h"
#include "Screen.h"
#include "Map.h"

using namespace std;

Player * Trigger::player = Player::Instance();

Trigger::Trigger(){}

Trigger::Trigger(const int ID, const string & message, const int create_inv_ID, const int create_ID, const int create_x, const int create_y)
		: ID(ID), message(message), create_inv_ID(create_inv_ID), create_ID(create_ID), create_x(create_x), create_y(create_y) {

	}

Trigger::~Trigger(){
	}

void Trigger::activate(const int object_x, const int object_y){
	Screen::status_bar(message);
	if(create_inv_ID!=0)
		player->inventory_add(create_inv_ID);
	if(create_ID!=0){
		if(create_x==0 && create_y==0)
			Map::at(object_x, object_y)=create_ID;		// KONTROLA
		 else
			 Map::at(create_x, create_y)=create_ID;		// KONTROLA
		}
	}