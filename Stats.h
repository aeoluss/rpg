#pragma once

///Structure for storing stats of an object
class Stats{

  public:
  	  ///Object hitpoints.
	  int hp;
	  ///Object attack strength.
	  int attack;
	  ///Object combat speed.
	  int speed;
	  ///Chances the object, when killed, will spawn random item. !Not implemented yet.
	  int droprate;

	///Default constructor.
	Stats();
	/**
	*@brief 			Constructor
	*@param hp			Hitpoints.
	*@param attack		Attack strength.
	*@param speed		Combat speed.
	*@param droprate	Droprate chance when killed.
	*@return
	*/
	Stats(int hp, int attack, int speed, int droprate);
	///Default destructor.
	~Stats();
	///Addition operator, combines (adds) stats.
	Stats & operator+=(const Stats & other);
};
