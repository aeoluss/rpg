//#include "stdafx.h"
#include <iostream>
#include "Icon.h"

using namespace std;


Icon::Icon(char * icon, const int char_size) : icon(nullptr), char_size(char_size) {
	if(char_size==0)return;
	this->icon = new char[char_size];
	for(int i=0; i<char_size; i++)
		this->icon[i]=icon[i];
	}


Icon::~Icon(){
	delete[] icon;
	}
/*
ostream & Icon::operator<<(ostream & os){
	for(int i=0; i<char_size; i++)
		os<<icon[i];
	return os;
	}
*/

Icon & Icon::operator=(const Icon & other){
	delete icon;
	icon=new char[other.char_size];
	char_size=other.char_size;
	for(int i=0; i<char_size; i++)
		icon[i]=other.icon[i];
	return *this;
	}

Icon::Icon() : icon(nullptr), char_size(0){}

Icon::Icon(const Icon & other) : icon(nullptr), char_size(other.char_size){
	icon=new char[other.char_size];
	for(int i=0; i<char_size; i++)
		icon[i]=other.icon[i];
	}