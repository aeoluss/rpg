#pragma once
#include <iostream>

using namespace std;

class Icon
{
public:
	char * icon;
	int char_size;

	Icon();
	Icon(const Icon & x);
	Icon(char * icon, const int char_size);
	~Icon();
	inline friend ostream & operator<< (ostream & os, const Icon & x){
		for(int i=0; i<x.char_size; i++)
			os<<x.icon[i];
		return os;
		}
	Icon & operator=(const Icon & other);
};

