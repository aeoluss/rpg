#include "Stats.h"
using namespace std;

Stats::Stats() : hp(0), attack(0), speed(0), droprate(0) {}

Stats::Stats(int hp, int attack, int speed, int droprate) : hp(hp), attack(attack), speed(speed), droprate(droprate) {
	}

Stats::~Stats(){
	}

Stats & Stats::operator+=(const Stats & other){
	hp+=other.hp;
	attack+=other.attack;
	speed+=other.speed;
	return *this;
	}