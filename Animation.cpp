#include "Animation.h"
//#include <algorithm>
#include <curses.h>
#include <fstream>
#include <unistd.h>

using namespace std;


	map <string, Video> Animation::videos = map <string, Video> ();
	string Animation::loaded_animation = "new_game";
	string Animation::return_command = "menu";

Animation::Animation (){
}

Animation::~Animation (){
}

void Animation::load (const string & command){
	loaded_animation=(command.substr (0, command.find_first_of('-')) );
	return_command=(command.substr (command.find_first_of('-')+1, command.length()-command.find_first_of('-')-1) );
	}

bool Animation::load_file (const string & fname){

	string title, message;
	string line;
	string videofname;
	ifstream file;
	file.open (fname.c_str(), fstream::in);
	if(!file.is_open())
		return false;
	videos.clear();
	
	std::getline(file, line);
	std::getline(file, line);

	while( !file.eof() ){
		videofname=fname;
		getline(file, line);
		if(line.length()<1)
			continue;
		if(line[line.length()-1]=='\n')
			line.resize(line.length()-1);
		if(line[line.length()-1]=='\r')
			line.resize(line.length()-1);
		videofname.resize(videofname.length()-4);
		videofname+="/";
		videofname+=line;
		videofname+=".txt";
		Video newVideo;
		newVideo.load_file(videofname);
		videos.insert(pair <string, Video> (line, newVideo) );
        }

	file.close();
	return true;
	}

bool Animation::save_file (const string & fname){
	map <std::string, Video>::iterator it;
	string message;
	string newlinecharacter;
	newlinecharacter+=(char)(' '-(char)31);
	ofstream file;
	file.open(fname.c_str());
	// KONTROLA OTVORENIA SUBORU
	file<<"delimiter is ';' newline character is ASCII char 1: '"+newlinecharacter+"'\r\ntitle;line1"+newlinecharacter+"line2\r\n";
	file<<"0 0 0\r\n";
	for(it=videos.begin(); it!=videos.end(); ++it)
		file<<it->first<<"\r\n";
	/*
	for(it=animations.begin(); it!=animations.end(); ++it)
		message=it->second;
		std::replace(message.begin(), message.end(), '\n', char(1));
		file<<(it->first)+";"+message+"\r\n";
	*/
	file.close();
	return true;
	}

string Animation::activate(){
	map <std::string, std::string>::iterator it;
	int i;
	list<string> lines;
	char c;
	int center_x, center_y;
	int offset_x, offset_y;
	string line, text;
	string wtf;
	center_x = WIDTH/2;
	center_y = HEIGHT/2;

	if (videos.find(loaded_animation)==videos.end())
		return return_command;
	Video loaded_video = videos.find(loaded_animation)->second;

	nodelay(stdscr,TRUE);		//non-blocking
	for (list <list <string> > ::iterator itB=loaded_video.frames.begin(); itB!=loaded_video.frames.end(); ++itB){
		offset_y=-((loaded_video.y_size)/2);
		clean();
		i=0;
		for(list<string>::iterator it=itB->begin(); it!=itB->end(); ++it, i++){
			offset_x=-(it->length())/2;
			printline(center_y+offset_y+i, (*it), center_x+offset_x);
			}
		draw();
		struct timespec timespec_key_delay;
	    timespec_key_delay.tv_sec = (1000/24/loaded_video.fps / 1000);
	    timespec_key_delay.tv_nsec = (((int)(1000/24/loaded_video.fps) % 1000) * 1000000);
		for(int xz=0; xz<24; xz++){
			c=getch();
			nanosleep(&timespec_key_delay, nullptr);
			if(c==27)
				break;
			}
		}
	
	nodelay(stdscr,FALSE);		//blocking again
	flushinp();
	return return_command;
	}