#pragma once
#include "Object.h"
#include <map>

///Object serialization class, stores objects in file database
class Objects{

public:
	///Map of objects, key value is object ID.
	static std::map <int, Object> objects;
	///Default constructor
	Objects();
	///Default destructor
	~Objects();

	/**
	*@brief 		Loads data from file.
	*@param fname 	Relative path to file.
	*@return		Success reading file.
	*/
	static bool load_file (const std::string & fname);
	/**
	*@brief 		Writes data to file.
	*@param fname 	Relative path to file.
	*@return		Success writing file.
	*/
	static bool save_file (const std::string & fname);
	/**
	*@brief 	Finds object, based on ID integer value from the database.
	*@param ID	ID of the object to be found.
	*@return	Object pointer, for better performance, and in case its stats may be needed to change (e.g. player HP).
	*/
	static Object * find_ID (const int ID);
	
};

