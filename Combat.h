#pragma once
#include "Screen.h"
#include "Player.h"
#include "Stats.h"
#include <string>
#include <map>

///Combat screen, interactive player combat
class Combat : public Screen{
  public:

	///Class Player singleton instance.
	static Player *player;
	///Player stats, used in combat.
	static Stats * player_stats;
	///Enemy stats copy, used in combat, so the original object doesn't change.
	static Stats enemy_stats;
	///Speed of the falling chars.
	static int difficulty_speed;
	///Density of the falling chars.
	static int difficulty_density;
	///Height for the chars to fall.
	static int difficulty_height;
	///Width for the chars to fall from.
	static int difficulty_width;
	///Map representing falling chars, char is the key and pair represents coordinates of the char.
	static std::map <char, std::pair<int, int> > falling_chars;

	///Default constructor.
	Combat ();
	///Default destructor.
	~Combat ();
	std::string activate ();
	///Shows player and enemy HP on screen.
	void show_health ();
	/**
	*@brief			Reads player input and represents it as hits (deleting corresponding chars).
	*@param evade	Turns true if player hits ESC (player evades from battle).
	*/
	void player_hits (bool & evade);
	///Updates players stats (the damage he inflicted in battle).
	void update_player_stats ();

};

