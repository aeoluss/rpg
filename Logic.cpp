#include "Logic.h"
#include "Menu.h"
#include "Game.h"
#include "Combat.h"
#include "Inventory.h"
#include "Objects_list.h"
#include "Animation.h"
#include <curses.h>

using namespace std;


Logic::Logic(){
	menu = new Menu();
	game = new Game();
	combat = new Combat();
	animation = new Animation();
	inventory = new Inventory();
	objects_list = new Objects_list();
}


Logic::~Logic(){
}

void Logic::start(){
	string command = "menu";
	initscr();
	//wresize(stdscr, getmaxy(stdscr), getmaxx(stdscr));
	//wresize(stdscr, getmaxy(stdscr), getmaxx(stdscr)-6);
	keypad (stdscr, true);
	noecho();
	wrefresh(stdscr);

	while (command != "exit"){
		if (command == "menu"){
			command=menu->activate();
			}
		if (command == "game"){
			command=game->activate();
			}
		if (command == "combat"){
			command=combat->activate();
			}
		if (command == "inventory"){
			command=inventory->activate();
			}
		if (command == "objects"){
			command=objects_list->activate();
			}
		if (command.substr(0,9) == "animation"){
			Animation::load(command.substr(10, command.length()-10) );
			command=animation->activate();
			}
		if (command == "dialog"){	// NOT SUPPORTED YET
			//dialog->activate();
			}
		if (command == "death"){
			Menu::game_loaded=false;
			command = "animation-death-menu";
			}
		/*if(command == "exit"){
			Animation::load("dance-menu");
			command=animation->activate();
			}
		*/

		}

	endwin();



	}
