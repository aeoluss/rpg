#pragma once
#include "Trigger.h"
#include <iostream>
#include <map>
using namespace std;

///Serialization for trigger database
class Triggers{
  public:

	static map <int, Trigger> triggers;

	Triggers();
	~Triggers();	
	/**
	*@brief 		Loads data from file.
	*@param fname 	Relative path to file.
	*@return		Success reading file.
	*/
	static bool load_file (const string & fname);
	/**
	*@brief 		Writes data to file.
	*@param fname 	Relative path to file.
	*@return		Success writing file.
	*/
	static bool save_file (const string & fname);

	static Trigger * find_ID (const int ID);

	static void activate (const int ID, const int object_x, const int object_y);

};

