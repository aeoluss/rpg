//#include "stdafx.h"
#include "Menu.h"
#include "Objects.h"
#include "Triggers.h"
#include "Map.h"
#include "Animation.h"
#include <curses.h>
#include <string>
#include <cstdlib>

using namespace std;

bool Menu::game_loaded=false;


Menu::Menu() : Screen(){
	//game_loaded=false;
}


Menu::~Menu(){
}

string Menu::activate(){
	string save_file_name="";
	string load_file_name="";
	char com=' ';
	Player * player = Player::Instance();
	clean();
	printline(0,"1\tcontinue game");
	printline(1,"2\tnew game");
	printline(2,"3\tload game");
	printline(3,"4\tsave game");
	printline(4,"5\texit");
	draw();
	com=getch();
	while(com!='1' && com != '2' && com!='3' && com!='4' && com!='5')
			com=getch();
//---------- continue game ----------//
	if(com=='1'){
		if(game_loaded)
			return "game";
		clean();
		printline(0,"no game loaded");
		draw();
		getch();
		return "menu";
		}
//---------- new game ----------//
	if(com=='2'){
		game_loaded=false;
		if( Objects::load_file ("files/new_game/objects.txt") == false){
			dialog("Files corrupt."); return "menu"; }
		if( Triggers::load_file ("files/new_game/triggers.txt") == false){
			dialog("Files corrupt."); return "menu"; }
		if( Map::load_file ("files/new_game/map.txt") == false){
			dialog("Files corrupt."); return "menu"; }
		if( Animation::load_file ("files/new_game/animations.txt") == false){
			dialog("Files corrupt."); return "menu"; }
		player->inventory.clear();
		Menu::game_loaded=true;
		return "animation-intro-game";
		}
//--------- load game ----------//
	if(com=='3'){
		game_loaded=false;
		clean();
		printline(0, "");
		load_file_name= dialog("enter folder name to load");
		if( Objects::load_file ("files/"+load_file_name+"/objects.txt") == false){
			dialog("File not found."); return "menu"; }
		if( Triggers::load_file ("files/"+load_file_name+"/triggers.txt") == false){
			dialog("File not found."); return "menu"; }
		if( Map::load_file ("files/"+load_file_name+"/map.txt") == false){
			dialog("File not found."); return "menu"; }
		if( Animation::load_file ("files/"+load_file_name+"/animations.txt") == false){
			dialog("File not found."); return "menu"; }
		Menu::game_loaded=true;
		return "game";
		}
//--------- save game ----------//
	if(com=='4'){
		if(!Menu::game_loaded)
			return "menu";
		save_file_name= dialog("enter save folder file name");
		system(string("mkdir files/\"").append(save_file_name).append("\"").c_str() );
		if( Objects::save_file ("files/"+save_file_name+"/objects.txt") == false)
			dialog("Error writing files, check user privileges.");
		if( Triggers::save_file ("files/"+save_file_name+"/triggers.txt") == false)
			dialog("Error writing files, check user privileges.");
		if( Map::save_file ("files/"+save_file_name+"/map.txt") == false)
			dialog("Error writing files, check user privileges.");
		if( Animation::save_file ("files/"+save_file_name+"/animations.txt") == false)
			dialog("Error writing files, check user privileges.");
		clean();
		return "menu";
		}
//----------- EXIT -------------//
	if(com=='5'){
		clean();
		printline(0, "are you sure you want to exit?");
		printline(1, "1\tyes");
		printline(2, "2\tno");
		draw();
		com=getch();
		while(com!='1' && com != '2')
			com=getch();
		if(com=='1')
			return "exit";
		 else
			 return "menu";
		}

	return "exit";	// NEDOSIAHNUTELNE
	}
