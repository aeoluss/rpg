﻿delimiter is ';' newline character is ASCII char 1: ''
13 0 1 0.3
You wake up.

Instead of usual pancakes, you smell pine.
You're cold. When you open your eyes, you start to think you're just now dreaming.
But there's no way of knowing, dreams are like that.

Last thing you remember is going to bed early in the morning..
after finishing another one of those bloody progtest assignments.

Is this it? Have you finally gone insane?
Either way, let's give it a go.

You see a sword lying not too far away..