#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <cstdlib>
#include "editor.h"

#ifdef _WIN64
    #define CLS_COMMAND system("cls");
#elif _WIN32
    #define CLS_COMMAND system("cls");
#elif __linux
    #define CLS_COMMAND system("clear");
#elif __unix
    #define CLS_COMMAND system("clear");
#elif __posix
    #define CLS_COMMAND system("clear");
#else
    #define CLS_COMMAND system("clear");
#endif

using namespace std;
/**
*@file editor.cpp
*@brief Converts map.ed in corresponding save folder and generates map in the same save folder.
*map.ed represents the map in chars (as the player will see it), reads the objects.txt database, assigns 
*every char its object ID and generates map.txt file, which can be used by the application.
*This is used for development, as using only chars as representation would render useless unimorphism of the objects 
*(single char can represent different objects), for example allies look the same, albeit they have different behavior. 
*Chars representing the objects can be then changed in the objects.txt database, obscuring the map design pattern for the player.
*/
int main(void){
	ifstream infile;
	ofstream outfile;
	ifstream objfile;
	istringstream iss;
	map<string, int> objects;
	string icon;
	int ID, x, y;
	map<string, int>::iterator it;
	char c;

	string save;
	string line;
RESTART:	
	CLS_COMMAND
	fflush(stdout);
	cout<<"select save to convert, type exit for exit"
	  <<endl<<"WARNING: original map file will be overwritten"<<endl;
	getline(cin, save);
	if(save=="exit" || save=="quit")
		return 0;
	
	objfile.open("files/"+save+"/objects.txt");
	if(!objfile.good())
		goto FNF;
	getline(objfile, line);
	while(!objfile.eof()){
		getline(objfile, line);
		if(line.length()<1)
			continue;
		iss.clear();
		iss.str(line);
		iss>>skipws>>ID>>skipws>>icon;
		objects.insert( pair<string, int>(icon, ID) );
		}
	objfile.close();


	infile.open("files/"+save+"/map.ed");
	if(!infile.good())
		goto FNF;
	getline(infile, line);
	getline(infile, line);
	iss.clear();
	iss.str(line.c_str());
	iss>>skipws>>x>>skipws>>y;
	outfile.open("files/"+save+"/map.txt");
	if(!outfile.good())
		goto NOWRITE;
	outfile<<"x,y map size\r\n";
	outfile<<x<<" "<<y<<"\r\n";

	for(int j=0; j<y && !infile.eof(); j++){
		getline(infile, line);
		if(line.length()<1)
			continue;
		iss.clear();
		iss.str(line);
		for(int i=0; i<x && !iss.eof(); i++){
			iss>>c;
			if(c=='\r' && c=='\n' && c=='\t')
				continue;
			icon.clear();
			icon+=c;
			it=objects.find(icon);
			if(it==objects.end()){
				cout<<"error: symbol '"+icon+"' from .ed file not found in objects.txt file"<<endl;
				getline(cin,line);
				//getch();
				goto CLOSE;
				}
			outfile<<it->second<<"\t";
			}
		outfile<<"\r\n";
		}
	cout<<"conversion succesfull"<<endl;
	getline(cin,line);
CLOSE:
	infile.close();
	outfile.close();
	outfile.close();
	goto RESTART;
FNF:
	cout<<"error: save not found"<<endl;
	getline(cin,line);
	objfile.close();
	infile.close();
	outfile.close();
	goto RESTART;
NOWRITE:
	cout<<"error: write unsuccesfull\r\ncheck user privileges"<<endl;
	getline(cin,line);
	outfile.close();
	infile.close();
	goto RESTART;

}