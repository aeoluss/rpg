#pragma once
#include <string>

struct Column;

/**
*@brief 	Parent screen class.
*Parent screen class, implements displaying functions used by all windows.
*/
class Screen{

  public:
	///Height of the terminal window.
	static int HEIGHT;
	///Width of the terminal window.
	static int WIDTH;		// DOPLN CONST
	///Subclass-shared display matrix.
	static char ** matrix;
	///Message displayed on status_bar (bottom of the screen).
	static std::string message1;
	///Message displayed on status_bar (bottom of the screen).
	static std::string message2;
	///Default constructor.
	Screen ();
	///Virtual destructor.
	virtual ~Screen ();
	///Displays matrix on to screen.
	void draw () const;
	///Clears matrix of all characters with blank space.
	void clean ();
	/**
	*@brief			Prints a single line of text on to matrix.
	*@param	line	No. of line to print the string on.
	*@param	text	String to be printed on the corresponding line.
	*@param	offset	(optional) Horizontal offset of the string (no. of chars skipped).
	*/
	static void printline (const int line, const std::string & text, const int offset = 0);
	/**
	*@brief 		User input dialog.
	*@param text	Text to be displayed to user.
	*				Prints text on screen and reads (single line) input from user.
	*@return		Line from user.
	*/
	std::string dialog (const std::string & text);
	/**
	*@brief			[] operator to access matrix element (char).
	*@param n		x coord of the desired element of the matrix.
	*@return		Helper struct Column.
	*/
	inline Column operator[] (const int n);
	//inline Column Screen::operator[] (const int n);
	/**
	*@brief			Virtual method used to select active window.
	*@return		Command to execute (mostly which window to activate next).
	*/
	virtual std::string activate () = 0;
	/**
	*@brief			Prints message on the bottom 2 lines of the matrix.
	*@param message	Message to be printed on to matrix.
	*/
	static void status_bar(const std::string & message);
	
};