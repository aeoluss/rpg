#pragma once
#include <string>
#include <list>

///Video screen, used to play animation or text
class Video{
public:

	int no_frames, x_size, y_size;
	double fps;
	std::list<std::list<std::string>> frames;
	std::list<std::string> frame;

	Video();
	~Video();
	/**
	*@brief 		Loads animation from file.
	*@param fname 	Relative path to file.
	*@return		Success reading file.
	*/
	bool load_file(const std::string & fname);
	/**
	*@brief 		Saves animation to file.
	*@param fname 	Relative path to file.
	*@return		Success writing file.
	*/
	bool save_file(const std::string & fname);
};

