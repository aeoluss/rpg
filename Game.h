#pragma once
#include "Screen.h"
#include "Object.h"

///Main game cycle with logic acting as state automata, switching between events
class Game : public Screen{
  public:

  	///Delay between keystrokes, preventing fast movement.
	static int key_delay;
	///Distance for the objects to be rendered, measured from the player.
	static int fog;
	///Ratio constant, to help render fog circular (terminal chars are tall).
	static int ratio_width;
	///Ration constant, to help render fog circular (terminal chars are tall).
	static int ratio_height;
	///Indicator whether combat was succesfull, or the player evaded.
	static bool combat_succesfull;
	///Help object pointer.
	Object * object;
	///Player x coordinate on the map.
	static int player_x;
	///Player y coordinate on the map.
	static int player_y;
	///Screen center/focus coordinates.
	static int focus_x, focus_y;
	static int offset_x, offset_y;
	static int cursor_x, cursor_y;
	///Indicator whether player is in first fight (displays longer combat intro animation).
	static bool first_fight;

	///Default constructor.
	Game();
	///Default destructor.
	~Game();
	std::string activate();
	///Toggle action/trigger on an object (when player interacts with other objects).
	void action(Object * object, const int object_x, const int object_y);
};

