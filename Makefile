CC=g++ -std=c++11
# Hey!, I am comment number 2. I want to say that CFLAGS will be the
# options I'll pass to the compiler.
CFLAGS=-c -Wall -g -O0 -Wno-long-long -pedantic
CLIBS= -lncurses
# -lcurses
LDFLAGS=
SOURCES=rpg.cpp Logic.cpp Object.cpp Objects.cpp Triggers.cpp Screen.cpp Menu.cpp Player.cpp Stats.cpp Trigger.cpp Map.cpp Game.cpp Combat.cpp Animation.cpp Video.cpp Inventory.cpp Objects_list.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=game.exe
EXECUTABLE2=editor.exe

#all: $(SOURCES) $(EXECUTABLE) editor.exe
all: $(SOURCES) $(EXECUTABLE) editor.exe doc

editor.exe: editor.cpp
	$(CC) $(LDFLAGS) editor.cpp -o editor.exe $(CLIBS)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ $(CLIBS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@ $(CLIBS)

clean:
	rm -rf *o game.exe editor.exe html latex
#	rm -rf game.exe editor.exe html latex

compile: $(EXECUTABLE)

doc:
	doxygen Doxyfile $(SOURCES) $(EXECUTABLE) editor.cpp

run:
	./game.exe

