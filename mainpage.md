<h1>RPG - role playing terminal adventure game</h1>

Installation instructions - see INSTALL.txt

rpg.exe:
    game executable
editor.exe:
    used for converting <custom_map>.ed into <custom_map>.txt which is used in game

save files are stored in /files/<save_name>/*

<h2>Screenshots</h2>
<img src="../map.png">

For bug reporting, please send e-mail to peter@mikolaj.sk
