#include "Player.h"
#include "Object.h"
#include "Objects.h"
#include <curses.h>
#include "Inventory.h"
#include <sstream>

using namespace std;


Inventory::Inventory(){
}

Inventory::~Inventory(){
}

string Inventory::activate(){
	Player * player = Player::Instance();
	int i=0;
	char com;
	string line;
	Object * object;
	clean();
	for(vector<int>::iterator it = player->inventory.begin(); it!=player->inventory.end(); ++it){
		ostringstream oss;
		oss<<i++<<": ";
		object=Objects::find_ID(*it);
		bool l1, l2, l3;
		l1=object->stats.hp!=0;
		l2=object->stats.attack!=0;
		l3=object->stats.speed!=0;
		oss.clear();
		//if(l1 || l2 || l3) 
		oss<<object->name;
		if(l1) oss<<"   HP: +"<<object->stats.hp;
		if(l2) oss<<"   attack: +"<<object->stats.attack;
		if(l3) oss<<"    speed: +"<<object->stats.speed;
		line=oss.str();
		printline(i, line);
		}
	
	ostringstream oss;
	oss.clear();
	oss.str("");
	oss<<"HP: "<<player->stats->hp;
	line=oss.str();
	printline(1, line, WIDTH/2);
	oss.clear();
	oss.str("");
	oss<<"attack: "<<player->stats->attack;
	line=oss.str();
	printline(2, line, WIDTH/2);
	oss.clear();
	oss.str("");
	oss<<"speed: "<<player->stats->speed;
	line=oss.str();
	printline(3, line, WIDTH/2);
	
	draw();
	
	com=getch();
	if(com==(char)27)
		return "game";
	if(com-48>=0 && com-48<=9){
		if( com-48<0 || (unsigned)com-48>=player->inventory.size() )
			return "inventory";
		object=Objects::find_ID(player->inventory[com-48]);
		(*(player->stats))+=object->stats;
		player->inventory.erase(player->inventory.begin()+com-48);
		return "inventory";
		}
	
	return "game";
	}
